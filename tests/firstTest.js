describe('Menu Verification', function () {
    test('Menu Verification', function (browser) {
        const urls = [
            "https://www.designory.com",
            "https://www.designory.com/work",
            "https://www.designory.com/about",
            "https://www.designory.com/careers",
            "https://www.designory.com/locations",
            "https://www.designory.com/contact",
            "https://www.designory.com/news"
        ]

        for (i = 0; i <= urls.length; i++) {
            browser
                .useCss()
                .url(urls[i])
                .waitForElementVisible('body')
                .useXpath()
                .assert.elementPresent('//a[text()="WORK"]')
                .assert.elementPresent('//a[text()="ABOUT"]')
                .assert.elementPresent('//a[text()="CAREERS"]')
                .assert.elementPresent('//a[text()="LOCATIONS"]')
                .assert.elementPresent('//a[text()="CONTACT"]')
                .assert.elementPresent('//a[text()="NEWS"]')
        }
        browser.end();
    })
});
