describe('Location Verification', function () {
  test('Location Verification', function (browser) {
    browser
      .url('https://www.designory.com/locations/chicago')
      .waitForElementVisible('body')
      .useXpath()
      .assert.visible('//h1[text()="CHI"]')
      .assert.elementPresent('//p[contains(text(),"Phone: +1 312 729 4500")]')
      .assert.cssProperty('//h2[text()="At such great heights."]', "font-size", "40px")
      .assert.attributeEquals('//img', 'src', 'https://maps.googleapis.com/maps/api/staticmap?center= 225 N Michigan Ave, Suite 700 Chicago, IL 60601&zoom=16.5&markers= 225 N Michigan Ave, Suite 700 Chicago, IL 60601&size=400x150&key=AIzaSyAkAXQMgbxLGj6ZFqVAAR8JT4-5LaWKfIY')
      .end();
  })
});