describe('Cookie Verification', function () {
    test('Cookie Verification', function (browser) {
        browser
            .url('https://www.designory.com')
            .useXpath()
            // Accept coockie
            .click('//button[text()="OK, GOT IT"]')
            
            .url('https://www.designory.com')
            .useXpath()
            // Verify cookie button not displayed
            .assert.not.visible('//button[text()="OK, GOT IT"]')
            // Delete visits cookie
            .deleteCookie("visits")
            .url('https://www.designory.com')
            .useXpath()
            // Verify cookie button is displayed
            .assert.visible('//button[text()="OK, GOT IT"]')
            .end();
    })
});