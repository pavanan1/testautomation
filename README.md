# TestAutomation



## Prerequisites
npm is already installed

## packages / libraries required
chromedriver
Selenium
nightwatch

## Configure project
Download / clone this project in your local
npm install

## Execute tests
npm test
